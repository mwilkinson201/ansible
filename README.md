Forked from Martin Eggen
  https://github.com/martineg/spacewalk-client

Ansible role for registering an Enterprise Linux system as a Spacewalk client.

Set variables *spw_server* and *spw_activation_key* (and override *spw_cert_rpm* if necessary) in _vars/main.yml_ before running.

Example playbook:

    - hosts: all
      become: true
      roles:
        - spacewalk-client

